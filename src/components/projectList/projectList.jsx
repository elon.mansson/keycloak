/* import axios from "axios"; */
import axios from "../../api/index";
import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import Project from "../project/Project";
import "./projectList.css";
import keycloak from "../../keycloak";
import { useUser } from "../../context/user-context";

const ProjectList = () => {
  const { state } = useLocation();
  const userData = useUser();
  const [projectList, setProjectList] = useState();

  useEffect(() => {
    if(userData.userData !== undefined){
      updateProjectAsUser();
    } else {
      axios
        .get("https://legalt.herokuapp.com/api/v1/projects")
        .then((res) => setProjectList(res.data))
        .catch(e => e)
    }
  }, []);

  const updateProjectAsUser = () => {
    axios
      .get(`https://legalt.herokuapp.com/api/v1/projects/${userData?.userData.uid}`)
      .then((res) => setProjectList(res.data))
      .catch(e => e)
  }

  return (
    <div className="main-container">
      <div className="project-container">
        {(projectList != null
            ? projectList
                .filter((posts) => {
                  console.log(state)
                  if (state?.test === "" && state?.tag === "" || state === null) {
                    return posts;
                  } else if (
                    state?.test !== "" &&
                    posts.title
                      .toLowerCase()
                      .includes(state?.test.toLowerCase())
                  ) {
                    return posts;
                  } else if (state?.tag !== "" && posts?.tag.toLowerCase().includes(state?.tag.toLowerCase())){
                    return posts;
                  }
                })
                .map((post) => {
                  return <Project props={post} key={post.id}/>;
                })
            : "")}
      </div>
    </div>
  );
};

export default ProjectList;
