import axios from "../../api/index";
import { useEffect } from "react";
import { useUser } from "../../context/user-context";
import { useState } from "react";
import Project from "../project/Project";

const History = () => {
    const { userData } = useUser();
    const [history, setHistory] = useState();

    useEffect(() => {
        axios.get(`https://legalt.herokuapp.com/api/v1/users/${userData.uid}/historyprojects`).then(res => setHistory(res.data))
    }, [])

    return (
        <div className="main-container">
        <div className="project-container">{history?.map(e => {return (<Project props={e.project}/>)})}</div>
        </div>
    )
}

export default History;