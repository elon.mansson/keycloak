import { Button, Checkbox, Modal } from "@mantine/core";
import { useEffect, useState } from "react";
import { useUser } from "../../context/user-context";
import keycloak from "../../keycloak";
import putMessage from "../../api/mutations/putMessage";
import "./profile.css";
import axios from "../../api/index";
import { useForm } from "react-hook-form";
import { useLocation } from "react-router-dom";

const ProfilePage = () => {
  const { userData, setUserData } = useUser();
  const [checked, setChecked] = useState();
  const [opened, setOpened] = useState(false);
  const [opened2, setOpened2] = useState(false);
  const [message, setMessage] = useState();
  const { state } = useLocation();
  const { register, handleSubmit, reset } = useForm();


  useEffect(() => { 
    if(state !== null) return;
    else {
      updateMessage();
      setChecked(userData.show)
    }
  }, []);

  const updateMessage = async (title) => {
    if (opened === true) setOpened(false);
    if (title?.title !== undefined) await putMessage(title.title, userData.uid);
    await axios
      .get(`https://legalt.herokuapp.com/api/v1/users/${userData.uid}/skills`)
      .then((res) => {
        setMessage(res.data);
      });
      reset();
  };

  const updateDescription = async (data) => {
    if (opened2 === true) setOpened2(false);
    await axios.put(
      `https://legalt.herokuapp.com/api/v1/users/${userData.uid}`,{show: checked, description: data.desc, uid: userData.uid });
    setUserData(undefined);
    reset();
  }

  const updateShow = async () => {
    setChecked(!checked);
    await axios.put(`https://legalt.herokuapp.com/api/v1/users/${userData.uid}`,{ show: !checked, description: userData.desc, uid: userData.uid });
    setUserData(undefined);
  }

  return (
    <>
      <div className="profile-container">
        <div>
          <div>
            <p><b>Name:</b> {state !== null ? <>{state.name}</> : <>{userData?.name}</>}</p>
          </div>
        </div>
        {state !== null ? '' : <div>
          <div>
            <p><b>Email:</b> {keycloak.tokenParsed.email}</p>
          </div>
        </div>}
        <div>
          <div>
            <p><b>Description:</b> {state !== null ? <>{state.description}</> : <>{userData?.description}</>}</p>
          </div>
          <Modal
            centered
            opened={opened2}
            onClose={() => setOpened2(false)}
            title="Update description"
          >
            <form
              className="modal-wrapper"
              onSubmit={handleSubmit(updateDescription)}
            >
              <div className="modal-container">
                <p>Description</p>
                <input
                  type="text"
                  placeholder="Description"
                  required
                  {...register("desc")}
                ></input>
              </div>
              <Button type="submit">Add</Button>
            </form>
          </Modal>
          {state !== null ? '' : <Button onClick={() => setOpened2(true)}>Update description</Button>}
        </div>
        {state !== null ? '' : <div>
          <div>
            <p><b>Show/Hide content: </b></p>
          </div>
          <Checkbox
            checked={checked}
            onChange={() => {updateShow()}}
          />
        </div>}
        
      </div>
      {(state !== null && state.show === true) ? '' : <div className="profile-container skill-container">
        <h3>Skills</h3>
        <div>
          {state !== null && state.show === true ? '' : state?.skills.map(e => {
            return(<p>{e.title}</p>)
          })}
          {message?.map((e) => {
            return <p>{e.title}</p>;
          })}
        </div>
        {state !== null ? '' : <Button
          onClick={() => {
            setOpened(true);
          }}
        >
          Add skill
        </Button>}
        
        <Modal
          centered
          opened={opened}
          onClose={() => setOpened(false)}
          title="Create Project"
        >
          <form
            className="modal-wrapper"
            onSubmit={handleSubmit(updateMessage)}
          >
            <div className="modal-container">
              <p>Skill title</p>
              <input
                type="text"
                placeholder="Title"
                required
                {...register("title")}
              ></input>
            </div>
            <Button type="submit">Add</Button>
          </form>
        </Modal>
      </div>}
      
    </>
  );
};

export default ProfilePage;
