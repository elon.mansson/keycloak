import "./Nav.css";
import { Link, useNavigate } from "react-router-dom";
import { Button, Paper } from "@mantine/core";
import { useState } from "react";
import keycloak from "../../keycloak";
import icon from "../../utils/icon.svg";
import DropDown from "../dropdownMenu/DropDown";
import { useForm } from "react-hook-form";
import { useUser } from "../../context/user-context";
import { useClickOutside } from '@mantine/hooks';
import { useEffect } from "react";
import axios from "../../api/index";

const Nav = () => {
  const { handleSubmit, reset } = useForm();
  const user = useUser();
  const [opened, setOpened] = useState(false);
  const [opened2, setOpened2] = useState(false);
  const [query, setQuery] = useState("");
  const navigate = useNavigate();
  const ref = useClickOutside(() => {setOpened(false); setOpened2(false)});
  const [projectList, setProjectList] = useState();
  let tagArray = [];
  let checkedAllTags = false;

  useEffect(() => {
    axios
      .get("https://legalt.herokuapp.com/api/v1/projects")
      .then((res) => setProjectList(res.data))
      .catch(e => e)
  }, []);

  const onSubmit = (data) => {navigate("/", { state: { tag: "", test: query }}); setQuery(); setOpened2(false)};

  return (
    <div className="navigation-container">
        <Link to="/" className="icon-container">
            <img src={icon} alt="icon" className="icon"></img>
            <span className="logo-text">LEGALT</span>
        </Link>
      <div className="search-container">
        <form onSubmit={handleSubmit(onSubmit)}>
          <input
            className="search"
            type="text"
            placeholder="Search Legalt"
            onClick={(e) => {e.target.value = ''; setOpened2(true);}}
            onChange={(e) => setQuery(e.target.value)}
          />
        </form>
      </div>
      {opened2 ? (
          <Paper className="tag-container" ref={ref}>
            {projectList?.map((e, i) => {
              if(!tagArray.includes(e.tag)) tagArray.push(e.tag);
              if(projectList.length - 1 === i) checkedAllTags = true;
            })}
            {checkedAllTags ? tagArray.map(e => {console.log(e);return (<div className="link" onClick={() => {
              navigate("/", { state: { tag: e, test: "" }});
              setOpened2(false)
            }}>{e}</div>)}) : ''}
          </Paper>
        ) : (
          ""
        )}
      
      <div className="profile">
        {user.userData === undefined ? <Button
          radius="xl"
          className="burger"
          onClick={() => keycloak.login()}
        >
          Login
        </Button> : <><Link to="/profile" className="profile-button">
          <Button radius="xl">Profile</Button>
        </Link><Button
          radius="xl"
          className="burger"
          onClick={() => setOpened((o) => !o)}
        >
          Menu
        </Button></>}
        

        {opened ? (
          <Paper className="drop-down-container" ref={ref}>
            <DropDown />
          </Paper>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default Nav;
