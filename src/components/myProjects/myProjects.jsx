import axios from "../../api/index";
import { useEffect } from "react";
import { useState } from "react";
import { useUser } from "../../context/user-context";
import { Button, Modal } from "@mantine/core";
import './myProject.css'
import { useForm } from 'react-hook-form'
import Project from "../project/Project";

const MyProjects = () => {
  const [projectList, setProjectList] = useState();
  const [opened, setOpened] = useState(false);
  const { userData } = useUser();
  const { register, handleSubmit } = useForm()

  useEffect(() => {
    if (userData !== undefined) {
      fetchProjects();
    }
  }, []);

  const fetchProjects = async () => {
    await axios
      .get(`https://legalt.herokuapp.com/api/v1/users/${userData.uid}/projects`)
      .then((res) => {
        setProjectList(res.data);
      })
      .catch((e) => e);
  };

  const createProject = async (data) => {
    setOpened(false);
    await axios.post("https://legalt.herokuapp.com/api/v1/projects", {
      title: data.title,
      desc: data.desc,
      gitRepo: data.gitRepo,
      tag: data.tag,
      admin: userData.uid,
      loggedInUsers: [userData.uid],
    });
    fetchProjects();
  };

  return (
    <div className="main-container">
    <div className="myProject-container">
        <h2>Create a new project</h2>
        <Modal
        centered 
        opened={opened}
        onClose={() => setOpened(false)}
        title="Create Project"
        >
      <form className="create-project"
        onSubmit={handleSubmit(createProject)}>
        <div>
          <p>title *</p>
          <input type="text" placeholder="title" required {...register('title')}></input>
        </div>

        <div>
          <p>desc *</p>
          <input type="text" placeholder="desc" required {...register('desc')}></input>
        </div>
        <div>
          <p>gitRepo</p>
          <input type="text" placeholder="gitRepo" {...register('gitRepo')}></input>
        </div>
        <div>
          <p>tag *</p>
          <input type="text" placeholder="tag" required {...register('tag')}></input>
        </div>
        <Button type="submit">Submit</Button>
      </form>
      </Modal>
      <Button onClick={() => setOpened(true)}>create a project</Button>
      
    </div>
    <div className="project-container">
    {projectList?.map((e) => {
        return (<Project props={e} key={e.id}/>);
      })}
      </div>
      </div>
  );
};

export default MyProjects;
