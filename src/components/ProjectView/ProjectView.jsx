import axios from "../../api/index";
import { useState } from "react";
import { useEffect } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { Textarea, Button, Modal } from "@mantine/core";
import "./ProjectView.css";
import { useUser } from "../../context/user-context";
import { useForm } from "react-hook-form";

const ProjectView = () => {
  const [project, setProject] = useState();
  const [users, setUsers] = useState();
  const [opened, setOpened] = useState(false);
  const [opened2, setOpened2] = useState(false);
  const [admin, setAdmin] = useState();
  const [userInProject, setUserInProject] = useState(false);
  const { state } = useLocation();
  const { register, handleSubmit, reset } = useForm();
  const { userData } = useUser();
  const navigate = useNavigate();
  
  useEffect(() => {
    console.log(userData)
    if(state === null) {navigate("/");}
    else {
      const test = async () => {
        await axios
        .get(`https://legalt.herokuapp.com/api/v1/projects/${state.id}/loggedin`)
        .then((res) => setProject(() => res.data));
        await axios
        .get(`https://legalt.herokuapp.com/api/v1/projects/${state.id}/users`)
        .then((res) => setUsers(() => res.data));
      }
      test();
    }
  }, []);
  

  useEffect(() => {
    if(project === undefined) return;
    console.log(userData?.uid, project?.admin, state)
    if (userData?.uid === project?.admin)
      {
        setUserInProject(true);
        axios
          .get(`https://legalt.herokuapp.com/api/v1/projects/${state.id}/admin`)
          .then((res) => setAdmin(res.data));
      } else {
        project?.requestUsers.map((e) => {
          if (e.uid === userData?.uid) {setUserInProject(true); console.log('true')};
        });
        project?.loggedInUsers.map((e) => {
          if (e === userData?.uid) {setUserInProject(true); console.log('true')};
        });
      }
  }, [project])

  const updateProject = () => {
    axios
      .get(`https://legalt.herokuapp.com/api/v1/projects/${state.id}/loggedin`)
      .then((res) => setProject(res.data));
  };

  const createMessage = async (data) => {
    await axios
      .post(`https://legalt.herokuapp.com/api/v1/message/${state.id}`, {
        comment: data.message,
        loggedInUser: userData.uid,
      })
      .then((res) => updateProject());
    reset();
  };

  const joinProject = async () => {
    await axios.put(
      `https://legalt.herokuapp.com/api/v1/projects/${state.id}/addrequest`,
      { loggedInUsers: [userData.uid] }
    );
    updateProject();
  };

  const test = (arr) => {
    if(arr !== undefined) arr.sort((a, b) => a.id - b.id);
    else {arr = []}
    return arr;
  }

  const changePicture = async (data) => {
    await axios.put(`https://legalt.herokuapp.com/api/v1/photos`, {url: data.url, project:state.id})
    updateProject();
  }

  const createProject = async (data) => {
    setOpened2(false);
    await axios.put(`https://legalt.herokuapp.com/api/v1/projects/${state.id}`, {
      title: data.title,
      desc: data.desc,
      gitRepo: data.gitRepo,
      tag: data.tag
    });
    updateProject();
  };

  return (
    <div className="project-view-container">
      <div className="project-description container">
        {userInProject === false && userData !== undefined ? (
          <><Button
            className="join-btn"
            radius="xl"
            onClick={() => {setOpened(true)
            }}
          >
            Join
          </Button>
          <Modal
          centered
          opened={opened}
          onClose={() => setOpened(false)}
          title="Agreement"
        >
          <div className="agreement-container">
            <p>Make sure you understand that the admin will be able to see all your information if you send in a request to join</p>
            <div>
            <Button onClick={() => {joinProject()}}>Accept</Button>
            <Button onClick={() => {setOpened(false)}}>Decline</Button>
            </div>
          </div>
        </Modal></>
        ) : 
          <>
            {admin?.admin === project?.admin ? 
            <Button
              className="join-btn"
              color="red"
              radius="xl"
              onClick={() => {axios.delete(`https://legalt.herokuapp.com/api/v1/projects/${state.id}`); navigate('/');
              }}
            >
              Delete
            </Button> : 
            ''}
          </>
        }
        <h1 className="margin">{project?.title}</h1>
        {project?.photos.length === 0 ? '' :
        <img
          className="img"
          alt="Img"
          src={project?.photos[0].url}
        ></img>}
        <p className="margin">
          <b>Project description:</b> {project?.desc}
        </p>
        <p className="margin">
          <b>Project git repository:</b> <a className="a-tag" href={project?.gitRepo}>{project?.gitRepo}</a> 
        </p>
        <div >
        {admin?.admin === project?.admin ? 
            <><form onSubmit={handleSubmit(changePicture)} className="url-container"><input type="text" placeholder="url for img" {...register("url")}></input><Button type="submit">Add picture url</Button></form></> : 
            ''}
        </div>
        <div className="tags margin">
          <b>Tags:</b> {project?.tag}
        </div>

        {admin !== undefined
          ? admin?.requestUsers.map((e) => {
              return (
                <div className="request-container">
                  <Button
                    onClick={() =>
                      axios
                        .put(
                          `https://legalt.herokuapp.com/api/v1/projects/${state.id}/handlerequest`,
                          { requestUsers: [e.uid], accept: true }
                        )
                        .then(checkIfAdmin)
                    }
                  >
                    Accept request
                  </Button>
                  <Button
                    onClick={() =>
                      axios
                        .put(
                          `https://legalt.herokuapp.com/api/v1/projects/${state.id}/handlerequest`,
                          { requestUsers: [e.uid], accept: false }
                        )
                        .then(checkIfAdmin)
                    }
                  >
                    Decline request
                  </Button>
                  <p>{e.name}</p>
                </div>
              );
            })
          : ""}
        <div className="project-members">
          <p>
            <b>Project members:</b>
          </p>
          {users?.map((e) => {
            return <p className="user-profile-button" onClick={() => {navigate('/profile', {state: e})}}>&nbsp;{e.name}</p>;
          })}
        </div>
        {admin?.admin === project?.admin ? 
        <><Button className="updateBtnMargin" onClick={() => setOpened2(true)}>Update</Button>
        <Modal
        centered 
        opened={opened2}
        onClose={() => setOpened2(false)}
        title="Update Project"
        >
      <form className="create-project"
        onSubmit={handleSubmit(createProject)}>
        <div>
          <p>title *</p>
          <input type="text" placeholder="title" required {...register('title')}></input>
        </div>

        <div>
          <p>desc *</p>
          <input type="text" placeholder="desc" required {...register('desc')}></input>
        </div>
        <div>
          <p>gitRepo</p>
          <input type="text" placeholder="gitRepo" {...register('gitRepo')}></input>
        </div>
        <div>
          <p>tag *</p>
          <input type="text" placeholder="tag" required {...register('tag')}></input>
        </div>
        <Button type="submit">Submit</Button>
      </form>
      </Modal></> : ''}
      </div>

      {userData ? <div className="message-board container">
        <div className="messages">
          <h3 className="title">Message board</h3>
          {project?.messages.length === 0 ? (
            <div className="message" key={project.id}>
              <p>Noone has written a message on this project yet!</p>
            </div>
          ) : (
            test(project?.messages).map((e) => {
              return (
                <div className="message">
                  <h3>{e.userName}</h3> <p>{e.comment}</p>
                </div>
              );
            })
          )}
          <form className="message-form" onSubmit={handleSubmit(createMessage)}>
            <Textarea
              className="message-input"
              placeholder="Autosize with no rows limit"
              autosize
              minRows={2}
              {...register("message")}
            />
            <Button radius="xl" type="submit">
              Send
            </Button>
          </form>
        </div>
      </div>
    : ''}
    </div> 
  );
};

export default ProjectView;
