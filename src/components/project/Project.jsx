import axios from "../../api/index";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/user-context";

const Project = (props) => {
  const navigate = useNavigate();
  const { userData, setUserData } = useUser();

  const addToHistory = async () => {
    console.log(userData)
    if(userData !== undefined){
      await axios.post(`https://legalt.herokuapp.com/api/v1/projects/${props.props.id}/addhistory`, {uid: userData?.uid})
      setUserData(undefined);
    }
    navigate(`/project/`, { state: { id: props.props.id } }); 
  }

  return (
    <div
      className="project"
      onClick={() => {addToHistory()}}
    >
      <h3>{props.props.title}</h3>
      {props.props.photos.length === 0 ? '' : <img className="img" alt="img" src={props.props?.photos[0].url}></img>}
      <p>{props.props.desc}</p>
      <h3>{props.props.tag}</h3>
    </div>
  );
};

export default Project;
