import { Link } from "react-router-dom";
import keycloak from "../../keycloak";
import "./dropdown.css";

const DropDown = () => {
  return (
    <>
      <ul className="dropdown">
        <Link to="/myProjects" className="link">
          <li>My projects</li>
        </Link>
        <Link to="/history" className="link">
        <li>History</li>
        </Link>
        <Link to="/profile" className="hide">
          <li>Profile</li>
        </Link>
        
        {keycloak.authenticated ? (
          <li onClick={() => {keycloak.logout();}}>Log out</li>
        ) : (
          <li onClick={() => {
            keycloak.login(); 
            navigator("/");
          }}>Log in</li>
        )}
      </ul>
    </>
  );
};

export default DropDown;
