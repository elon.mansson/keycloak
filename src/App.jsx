import "./App.css";
import Nav from "./components/nav/Nav";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import KeycloakRoute from "./routes/KeycloakRoute";
import ProfilePage from "./components/profile/profilePage";
import ProjectList from "./components/projectList/projectList";
import ProjectView from "./components/ProjectView/ProjectView";
import { ROLES } from "./const/roles";
import {ProviderContext} from "./context/user-context";
import MyProjects from "./components/myProjects/myProjects";
import History from "./components/history/history";

function App() {
  return (
    <ProviderContext>
      <BrowserRouter>
        <div className="App">
          <Nav></Nav>
          <div className="main-container">
            <Routes>
              <Route path="/" element={<ProjectList />}></Route>
              <Route path="/myProjects" element={<KeycloakRoute role={ROLES.User}><MyProjects/></KeycloakRoute>}></Route>
              <Route
                path="/profile"
                element={
                  <KeycloakRoute role={ROLES.User}>
                    <ProfilePage />
                  </KeycloakRoute>
                }
              ></Route>
              <Route path="/project" element={<ProjectView />}></Route>
              <Route path="/history" element={<KeycloakRoute role={ROLES.User}><History /></KeycloakRoute>}></Route>
            </Routes>
          </div>
        </div>
      </BrowserRouter>
    </ProviderContext>
  );
}

export default App;
