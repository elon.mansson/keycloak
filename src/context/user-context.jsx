import axios from "../api/index";
import { useState } from "react";
import { useContext } from "react";
import { createContext } from "react";
import keycloak from "../keycloak";

const userContext = createContext();

export function ProviderContext({children}) {
    const [userData, setUserData] = useState();
    
    const test = () => {if (userData === undefined && keycloak.token != null) {axios.get(`https://legalt.herokuapp.com/api/v1/users/${keycloak.tokenParsed.sub}`)
        .then((res) => {
            setUserData(res.data);
        }).catch(e => createUser());
    }};

    test();

    const createUser = () => {axios.post(`https://legalt.herokuapp.com/api/v1/users/register`)
        .then(res => test());
    }

    return (<userContext.Provider value={{userData, setUserData}}>
        {children}
    </userContext.Provider>);
}

export const useUser = () => {
    return useContext(userContext)
}
