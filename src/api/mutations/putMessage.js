import axios from "../index"

const putMessage = async (title, uid) => {
    const res = await axios.post(`https://legalt.herokuapp.com/api/v1/skills/${uid}/`, {title: title});
    return res;
}


export default putMessage